# Airport-Simulation

Project created for LO41 UTBM's UV.

The aim of the project is to use all the knowledge we learnt during the P20 semester about operating system in C
to create a simulation of a running airport : manage the differents landing tracks, authorization from the control tower or manage the different problem
an aicraft can encounter during landing. I used semaphores to control access and fork to create the aicrafts.

# Installation and Utilisation

To install the project you just need to clone the repository. To run it,
you first need to run **make** command and then use **./main** (if it doesn't work, check the permissions you've granted to the executable file)
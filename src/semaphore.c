#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/types.h>

#define SEMPERM 0600
#define IFLAGS (SEMPERM | IPC_CREAT)

/*  Opérators for P and V operations */
int semid;
struct sembuf sem_oper_P ;  /* Operation P */
struct sembuf sem_oper_V ;  /* Operation V */

/* Sémaphors init method */
int initSem(key_t semkey){
    
	int status = 0;		
	int semid_init;
   	union semun {
		int val;
		struct semid_ds *stat;
		short * array;
	} ctl_arg;
    if ((semid_init = semget(semkey, 6, IFLAGS)) > 0) {
		
	    	short array[6] = {0,0,0,0,1,1};
	    	ctl_arg.array = array;
	    	status = semctl(semid_init, 0, SETALL, ctl_arg);
    }
   if (semid_init == -1 || status == -1) { 
	perror("Erreur initsem");
	return (-1);
    } else return (semid_init);
}

/* Méthod to free the semaphors */
int libereSem(){
	return semctl(semid, 0, IPC_RMID);
}

/* P operation */
void P(int semnum) {
	sem_oper_P.sem_num = semnum;
	sem_oper_P.sem_op  = -1 ;
	sem_oper_P.sem_flg = 0 ;
	semop(semid,&sem_oper_P,1);
}

/* V operation */
void V(int semnum) {
	sem_oper_V.sem_num = semnum;
	sem_oper_V.sem_op  = 1 ;
	sem_oper_V.sem_flg  = 0 ;
	semop(semid,&sem_oper_V,1);
}

/* Method to get the number of process waiting */
int getSemSize(int semnum){
	return semctl(semid, semnum,GETNCNT);
}

/* Method to get the actual value of the semaphore */
int getSemVal(int semnum){
	return semctl(semid, semnum, GETVAL);
}




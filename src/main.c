#include <sys/wait.h>
#include <stdio.h> 
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h> 
#include <unistd.h> 
#include <string.h>

#include "semaphore.h"
#include "settings.h"
#include "airport.h"
#include "utils.h"

#define SKEY   (key_t) IPC_PRIVATE	
int semid;

/* Error printing method */
void erreur(const char* msg) {

  fprintf(stderr,"%s\n",msg);

}

/* Method to manage the programm end */
void traitantSIGINT(int num) { //on gère l'arrêt du programme

  if (num!=SIGINT){
    erreur("\nPb sur SigInt...");
  }
  libereSem();
  printf("\n---- Arrêt de la simulation ----\n");  
  exit(0);
}

int main(int argc, char *argv[])
{	
	/* If the user interupt the program, we compute it customly */
	signal(SIGINT,traitantSIGINT);
	
	/* Creating a group of semaphore */
	if ((semid = initSem(SKEY)) < 0)
		return(1);
	
	/* Base display */
	display(5, "	    Bienvenue dans l'interface de l'aéroport de Bâle-Mulhouse !", "[36m");
	display(0, "", "[0m");
	display(4, "1#Canal gros porteurs								2#Canal moyen porteurs", "[36m\033[4m");
	
	/* Loop to create aicraft. A new aicraft is created each NEW_AIRCRAFT seconds */
	while(1){
		aircraft();
		sleep(NEW_AIRCRAFT);	
	};
  
   return(0);
}

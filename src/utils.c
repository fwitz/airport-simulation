#include <stdio.h>
#include <stdlib.h>
#include "settings.h"

/* Method to display nicely the infos on the console. Color match to a specific escape sequence */
void display(int i, char* s, char* color) {
  
   int j, space;
   space=i*COLUMN;
   
   for (j=0; j<space; ++j){
   	putchar(' ');
   }
   printf("\033%s%s\033[0m\n",color,s);
   fflush(stdout);
}

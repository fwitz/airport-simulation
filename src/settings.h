#ifndef _SETTINGS_H
#define _SETTINGS_H

/* Destinations list */
extern const char* DESTINATIONS[];
				
/* Semaphores const */
#define STACK_L 0 /* Semaphore to control the little aircrafts with no problems */
#define STACK_L_PRIO 1 /* Semaphore to controle the little aircrafts with problems */
#define STACK_B 2 /* Semaphore to control the big aircrafts with no problems */
#define STACK_B_PRIO 3 /* Semaphore to controle the big aircrafts with problems */
#define RUNWAY_L 4 /* Semaphore to control the runway of the little aircrafts */
#define RUNWAY_B 5 /* Semaphore to control the runway of the big aircrafts */

#define NEW_AIRCRAFT 8 /* Time interval between each aircraft creation */
#define TAKE_OFF_TIME 15 /* Duration of take off for each aircraft */
#define LANDING_TIME 10 /* Duration of landing for each aircraft */

#define COLUMN 15 /* Size of a column (in char), used for the display */

#endif

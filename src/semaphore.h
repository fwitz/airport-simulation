#ifndef _SEMAPHORE_H
#define _SEMAPHORE_H

#include <sys/types.h>
#include  <sys/ipc.h>

int initSem(key_t semkey);
int libereSem();
void P(int semnum);
void V(int semnum);
int getSemSize(int semnum);
int getSemVal(int semnum);

#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdio.h> 
#include <sys/types.h> 
#include <unistd.h> 
#include <time.h>
#include <string.h>
#include <errno.h>

#include "settings.h"
#include "semaphore.h"
#include "utils.h"

/* Struct representing a plane */
typedef struct s_aircraft Aircraft;
struct s_aircraft{
	/* Fly number */
	int fly_num;
	/* ID of the destination/departure city */
	int city_id;
	/* Gazoline level, generated randomly, between 0 and 100 % */
	int gazoline;
	/* Generated randomly, 0 for no problem, 1 otherwise (like motor problems or passenger passing out) */
	int problem;
	/* 0 for little aircraft and 1 for big aircraft */
	int model;
	/* Store if the aircraft is leaving (0) or coming (1) */
	int status;
};

/* Method to add an aircraft on the correct waiting stack before take off/landing */
void waiting(Aircraft aircraft){
	if(aircraft.model == 0){
		/* If the aircraft is in a critical situation we add it to the priority stack*/
		if(aircraft.problem == 1 || aircraft.gazoline <= 20){
			P(STACK_L_PRIO);
		}else{
			P(STACK_L);
		}
	}else{
		if(aircraft.problem == 1 || aircraft.gazoline <= 20){
			P(STACK_B_PRIO);
		}else{
			P(STACK_B);
		}
	}
}

/* Method that represent the control tower. Gives the order to go/come or not to each plane depending on if aircraft with technics
 problems are coming or not and if the runways are free. Return 0 if the aircraft has the authorization to go/come and 1 otherwise. */
int controlTower(Aircraft aircraft){
	char fly_number[5];
	char* txt = malloc(150); /* String to print info */
	int semval;
	
	sprintf(fly_number, "%d", aircraft.fly_num); /*Converting the flying number to string */
	display(0, "", "[0m");
	/* If the aircraft is comming */
	if(aircraft.status == 1){
		strcat(txt, "Vol n°");
		strcat(txt, fly_number);
		strcat(txt, " en provenance de ");
		strcat(txt, DESTINATIONS[aircraft.city_id]);
		strcat(txt," à TdC.Demande autorisation d'atterir. A vous.");
		/* We check if it's a little aircraft or a big */
		if(aircraft.model == 0){
			semval = getSemVal(RUNWAY_L);
			display(7, txt, "[35m"); 
		}else{
			semval = getSemVal(RUNWAY_B);
			display(0, txt, "[35m"); 
		}
		strcpy(txt, "\0");
		if(semval == 1){
			strcat(txt, "TdC à vol n°");
			strcat(txt, fly_number);
			strcat(txt, ". Autorisation d'engager la procédure d'atterissage. A vous.");
			if(aircraft.model == 0){
				display(8, txt, "[0m"); 
			}else{
				display(1, txt, "[0m"); 
			}
			free(txt);
			return 0;
		}else{
			if(aircraft.problem == 1 || aircraft.gazoline <= 20){
				strcat(txt, "TdC à vol n°");
				strcat(txt, fly_number);
				strcat(txt, ". Autorisation d'engager la procédure d'atterissage d'urgence. A vous.");
				if(aircraft.model == 0){
					display(8, txt, "[0m"); 
				}else{
					display(1, txt, "[0m"); 
				}
				free(txt);
				return 0;
			}else{
				strcat(txt, "TdC à vol n°");
				strcat(txt, fly_number);
				strcat(txt, ". Procédure d'atterissage impossible. Mettez vous en attente.");
				if(aircraft.model == 0){
					display(8, txt, "[0m"); 
				}else{
					display(1, txt, "[0m"); 
				}
				free(txt);
				return 1;
			}
		}
	}else{
		strcat(txt, "Vol n°");
		strcat(txt, fly_number);
		strcat(txt, " à destination de ");
		strcat(txt, DESTINATIONS[aircraft.city_id]);
		strcat(txt, " à TdC.Demande autorisation de décoller. A vous.");
		if(aircraft.model == 0){
			semval = getSemVal(RUNWAY_L);
			display(7, txt, "[33m"); 
		}else{
			semval = getSemVal(RUNWAY_B);
			display(0, txt, "[33m"); 
		}
		strcpy(txt, "\0");
		if(semval == 1){
			strcat(txt, "TdC à vol n°");
			strcat(txt, fly_number);
			strcat(txt, ".Autorisation d'engager la procédure de décollage. A vous.");
			if(aircraft.model == 0){
				display(8, txt, "[0m"); 
			}else{
				display(1, txt, "[0m"); 
			}
			free(txt);
			return 0;
		}else{
			strcat(txt, "TdC à vol n°");
			strcat(txt, fly_number);
			strcat(txt, ". Procédure de décollage impossible. Mettez vous en attente.");
			if(aircraft.model == 0){
				display(8, txt, "[0m"); 
			}else{
				display(1, txt, "[0m"); 
			}
			free(txt);
			return 1;
		}
	}
}

/* Method used by an aircraft after takeoff/landing to signal to the control power the sucess of the 
manuver. It unfreeze a waiting aircraft at the same time */
void communicateWithControlTower(Aircraft aircraft){
	char fly_number[5];
	char* txt = malloc(100);
	int semsize;

	sprintf(fly_number, "%d", aircraft.fly_num);
	if(aircraft.status == 0){
		strcat(txt, "Vol n°");
		strcat(txt, fly_number);
		strcat(txt, " à TdC. Décollage effectué avec succès. Terminé.");
	}else{
		strcat(txt, "Vol n°");
		strcat(txt, fly_number);
		strcat(txt, " à TdC. Atterissage effectué avec succès. Terminé.");
	}
	if(aircraft.model == 0){
		display(7, "", "[0m");
		display(7, txt, "[32m"); 
		V(RUNWAY_L);
		if(getSemSize(STACK_L_PRIO) > 0){
			V(STACK_L_PRIO);
		}else if(getSemSize(STACK_L) > 0){
			V(STACK_L);
		}
	}else{	
		display(0, "", "[0m");
		display(0, txt, "[32m"); 
		V(RUNWAY_B);
		if(getSemSize(STACK_B_PRIO) > 0){
			V(STACK_B_PRIO);
		}else if(getSemSize(STACK_B) > 0){
			V(STACK_B);
		}
	}
	free(txt);
}

/* Method that makes an aircraft taking off */
void takeOff(Aircraft aircraft, int order){
	char fly_number[5];
	char* txt = malloc(100);
	char* tdc = malloc(100);
	int semsize;

	sprintf(fly_number, "%d", aircraft.fly_num);
	if(order == 0){
		strcat(txt, "Vol n°");
		strcat(txt, fly_number);
		strcat(txt, " à TdC.Bien reçu. Lancement de la procédure de décollage.");
		if(aircraft.model == 0){
			display(7, txt, "[32m"); 
			P(RUNWAY_L);
		}else{
			display(0, txt, "[32m");
			P(RUNWAY_B); 
		}
		sleep(TAKE_OFF_TIME);
		communicateWithControlTower(aircraft);
	}else{
		strcat(txt, "Vol n°");
		strcat(txt, fly_number);
		strcat(txt, " à TdC.Bien reçu. En attente de nouvelles instructions.");
		if(aircraft.model == 0){
			display(7, txt, "[31m"); 
		}else{
			display(0, txt, "[31m");
		}
		waiting(aircraft);

		strcat(tdc, "TdC à vol n°");
		strcat(tdc, fly_number);
		strcat(tdc, ". Autorisation d'engager la procédure de décollage. A vous.");
		strcpy(txt, "");
		strcat(txt, "Vol n°");
		strcat(txt, fly_number);
		strcat(txt, " à TdC.Bien reçu. Lancement de la procédure de décollage.");
		if(aircraft.model == 0){
			display(7, "", "[0m");
			display(8, tdc, "[0m"); 
			display(7, txt, "[32m");
			P(RUNWAY_L);
		}else{
			display(0, "", "[0m");
			display(1, tdc, "[0m");
			display(0, txt, "[32m"); 
			P(RUNWAY_B); 
		}
		sleep(TAKE_OFF_TIME);
		communicateWithControlTower(aircraft);
	}
	free(txt);
}

/* Method that makes an aircraft landing */
void landing(Aircraft aircraft, int order){
	char fly_number[5];
	char* txt = malloc(100);
	char* tdc = malloc(100);
	int semsize;

	sprintf(fly_number, "%d", aircraft.fly_num);
	if(order == 0){
		strcat(txt, "Vol n°");
		strcat(txt, fly_number);
		strcat(txt, " à TdC.Bien reçu. Lancement de la procédure d'approche pour engager l'atterissage.");
		if(aircraft.model == 0){
			display(7, txt, "[32m"); 
			P(RUNWAY_L);
		}else{
			display(0, txt, "[32m");
			P(RUNWAY_B); 
		}
		sleep(LANDING_TIME);
		communicateWithControlTower(aircraft);
	}else{
		strcat(txt, "Vol n°");
		strcat(txt, fly_number);
		strcat(txt, " à TdC.Bien reçu. En attente de nouvelles instructions.");
		if(aircraft.model == 0){
			display(7, txt, "[31m"); 
		}else{
			display(0, txt, "[31m");
		}
		waiting(aircraft);
		display(0, "", "[0m");
		strcat(tdc, "TdC à vol n°");
		strcat(tdc, fly_number);
		strcat(tdc, ". Autorisation d'engager la procédure d'attérissage. A vous.");
		strcpy(txt, "");
		strcat(txt, "Vol n°");
		strcat(txt, fly_number);
		strcat(txt, " à TdC.Bien reçu. Lancement de la procédure d'approche pour engager l'atterissage.");
		if(aircraft.model == 0){
			display(7, "", "[0m");
			display(8, tdc, "[0m"); 
			display(7, txt, "[32m");
			P(RUNWAY_L);
		}else{
			display(0, "", "[0m");
			display(1, tdc, "[0m");
			display(0, txt, "[32m"); 
			P(RUNWAY_B); 
		}
		sleep(LANDING_TIME);
		communicateWithControlTower(aircraft);
	}
	free(txt);
}



/* Method to create a plane */
void aircraft(){
	Aircraft aircraft;
	int order;
	
	/* Initializing aircraft */
	srand(time(NULL));
	aircraft.fly_num = (rand()/(double)RAND_MAX) * (9999 - 1000 + 1) + 1000; /* Generating the fly number between 1000 and 9999 like fly n°7456 */
	aircraft.city_id = (rand()/(double)RAND_MAX) * 30; /* Choosing randomly the id of the destination/departure city */
	aircraft.status = (rand()/(double)RAND_MAX) * 2; /* Choosing if the aircraft is coming or leaving */
	aircraft.model = (rand()/(double)RAND_MAX) * 2; /* Choosing the model of the aircraft */
	
	if(aircraft.status == 1){
		aircraft.gazoline = (rand()/(double)RAND_MAX) * 101; /* Generating gazoline level between 0 and 100 % */
		aircraft.problem = (rand()/(double)RAND_MAX) * 2; /* Generating problems (or not) for the aircraft */
	}else{
		aircraft.gazoline = 100; /* The aircraft is taking off, tank has been filled by the airport crew */
		aircraft.problem = 0; /* The aircraft is taking off, problems check has been made by the airport crew */
	}
	
	/* Creating the child process */
	if(fork() == 0){
		order = controlTower(aircraft); /* Control Tower gives order to the commandant of the aircraft */
		/* Depending on the aircraft status, we make it take off or landing and we pass the control tower orders */
		if(aircraft.status == 0){
			takeOff(aircraft, order);
		}else{
			landing(aircraft, order);
		}
		exit(0);
	}
}

CC=gcc
CFLAGS=-W -Wall -ansi -pedantic
EXEC=main

all: mrproper $(EXEC)

main: semaphore.o airport.o utils.o settings.o main.o
	$(CC) -o main semaphore.o airport.o utils.o settings.o main.o

semaphore.o: semaphore.c
	$(CC) -o semaphore.o -c semaphore.c $(CFLAGS)

airport.o: airport.c semaphore.h settings.h utils.h
	$(CC) -o airport.o -c airport.c $(CFLAGS)

utils.o: utils.c settings.h
	$(CC) -o utils.o -c utils.c $(CFLAGS)

settings.o: settings.c settings.h
	$(CC) -o settings.o -c settings.c $(CFLAGS)

main.o: main.c semaphore.h settings.h airport.h utils.h
	$(CC) -o main.o -c main.c $(CLFAGS)

clean:
	rm -rf *.o

mrproper: clean
	rm -rf $(EXEC)


